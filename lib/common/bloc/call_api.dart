import 'package:demo_mvvm_flutter/common/widget/info_dialog.dart';
import 'package:demo_mvvm_flutter/common/widget/no_connection_dialog.dart';
import 'package:demo_mvvm_flutter/generated/l10n.dart';
import 'package:demo_mvvm_flutter/main.dart';
import 'package:demo_mvvm_flutter/model/http_raw/NetworkException.dart';
import 'package:demo_mvvm_flutter/utils/connectivity_helper.dart';
import 'package:demo_mvvm_flutter/utils/const.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

typedef ErrorCallback = void Function(NetworkException e);

bool isDialogShowing = false;

Future<void> handleCallApi({
  required AsyncCallback onCallApi,
  ErrorCallback? onNoInternet,
  ErrorCallback? onError,
  bool handleErrorCode = false,
  bool shouldShowDialogNotConnectionBeforeCallApi = true,
  bool shouldDefaultErrorDialogWhenCallApi = false,
  VoidCallback? actionReload,
  bool isDisplayReloadButtonInsteadOfOK = false,
}) async {
  final connectivityManager = getIt<ConnectivityHelper>();
  bool isNetworkConnected = await connectivityManager.isNetworkConnected();

  if (!isNetworkConnected) {
    if (shouldShowDialogNotConnectionBeforeCallApi) {

    }
    onNoInternet?.call(const NetworkException(code: Const.noInternet));
    return;
  }
  try {
    await onCallApi.call();
  } on NetworkException catch (e) {
    if (shouldDefaultErrorDialogWhenCallApi) {

    }
  } catch (e) {
    if (shouldDefaultErrorDialogWhenCallApi) {

      onError?.call(NetworkException(code: Const.unknownErrorNetworkCall, message: "test"));
    }
  }

  void _showNoConnectionDialog({Function()? action}) {
    final BuildContext? buildContext = navigatorKey.currentState?.context;
    if (buildContext != null && !isDialogShowing) {
      isDialogShowing = true;
      showDialog(
        barrierDismissible: false,
        barrierColor: Colors.transparent,
        context: buildContext,
        builder: (context) {
          return NoConnectionDialog(action: action);
        },
      ).then((_) => isDialogShowing = false);
    }
  }

  void _showDefaultErrorDialog({
    required String message,
    Function()? action,
    bool isDisplayReloadButtonInsteadOfOK = false,
  }) {
    final BuildContext? buildContext = navigatorKey.currentState?.context;
    if (buildContext != null && !isDialogShowing) {
      isDialogShowing = true;
      showDialog(
        context: buildContext,
        builder: (context) {
          return InfoDialog(
              action: isDisplayReloadButtonInsteadOfOK ? action : null,
              title: S.current.notice,
              content: message,
              textButtonAction: isDisplayReloadButtonInsteadOfOK
                  ? S.current.reload
                  : S.current.ok);
        },
      ).then((_) => isDialogShowing = false);
    }
  }

}
