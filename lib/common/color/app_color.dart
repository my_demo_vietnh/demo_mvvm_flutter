


import 'dart:ui';

class AppColor {
  static const white = Color(0xFFFFFFFF);
  static const black = Color(0xFF111111);
  static const red = Color(0xFFFF0000);
  static const brightGray = Color(0xFFF3F1ED);
  static const orangePeel = Color(0xFFFF9D00);
  static const quickSilver = Color(0xFF9F9F9F);
  static const blackOlive = Color(0xFF3C3C3C);
  static const antiFlashWhite = Color(0xFFF3F3F3);
  static const yellowOrange = Color(0xFFFFAA36);
  static const grey = Color(0xFFF4F4F4);
  static const greyBorder = Color(0xFF707070);
  static const lightGray = Color(0xFFD5D5D5);
  static const darkTangerine = Color(0xFFFDAD15);
  static const grayBorderContentText = Color(0xFFB7B7B7);
  static const grayBackgroundContentText = Color(0xFFEAEAEA);
  static const gray = Color(0xFF808080);
  static const quartz = Color(0xFF4A4A4A);
  static const yellowOrange33 = Color(0xFFFDAD33);
  static const sonicSilver = Color(0xFF747474);
  static const yellowOrange32 = Color(0xFFF9AA32);
  static const grayX11 = Color(0xFFBABABA);
  static const sonicSilver79 = Color(0xFF797979);
  static const gainsBoro = Color(0xFFDBDBDB);
  static const textColorSilver = Color(0xFF797979);
  static const orange = Color(0xFFFF8D4D);
}
