


class AppDimentions {
  static const smallMargin = 7;
  static const normalMargin = 14;
  static const bigMargin = 21;
  static const largeMargin = 28;
}