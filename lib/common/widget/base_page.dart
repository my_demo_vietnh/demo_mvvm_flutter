


import 'package:demo_mvvm_flutter/common/color/app_color.dart';
import 'package:demo_mvvm_flutter/utils/hideKeyboard.dart';
import 'package:flutter/material.dart';

class BasePage extends StatelessWidget {
  const BasePage({
    Key? key,
    required this.body,
    this.backgroundColor = AppColor.white,
    this.appBar,
    this.extendBodyBehindAppBar,
    this.resizeToAvoidBottomInset,
  }) : super(key: key);

  final Widget body;
  final Color backgroundColor;
  final AppBar? appBar;
  final bool? extendBodyBehindAppBar;
  final bool? resizeToAvoidBottomInset;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          hideKeyboard();
        },
        child: Scaffold(
          appBar: appBar,
          backgroundColor: backgroundColor,
          extendBodyBehindAppBar: extendBodyBehindAppBar ?? false,
          resizeToAvoidBottomInset: resizeToAvoidBottomInset,
          body: body,
        ));
  }
}
