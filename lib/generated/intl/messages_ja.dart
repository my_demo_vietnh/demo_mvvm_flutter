// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ja locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ja';

  static String m0(phoneNumber) =>
      "***-****-${phoneNumber}にお送りした6桁の認証 番号を入力してください。";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "account_info_text": MessageLookupByLibrary.simpleMessage("アカウント情報"),
        "account_registration_text":
            MessageLookupByLibrary.simpleMessage("アカウント登録"),
        "add_Account_Register":
            MessageLookupByLibrary.simpleMessage("招待コードをお持ちの方はご入力ください"),
        "address": MessageLookupByLibrary.simpleMessage("お住まいの市町村"),
        "and": MessageLookupByLibrary.simpleMessage("と"),
        "and_text": MessageLookupByLibrary.simpleMessage("と"),
        "birthday": MessageLookupByLibrary.simpleMessage("お誕生月"),
        "boy": MessageLookupByLibrary.simpleMessage("男性"),
        "buy": MessageLookupByLibrary.simpleMessage("取得済み"),
        "buy_honda_project_explain": MessageLookupByLibrary.simpleMessage(
            "提供するショップで、クーポンのサービスを受けてください。"),
        "cancel": MessageLookupByLibrary.simpleMessage("キャンセル"),
        "cancel_your_account_text":
            MessageLookupByLibrary.simpleMessage("アカウントを解約する"),
        "change_text": MessageLookupByLibrary.simpleMessage("変更"),
        "check_honda_project": MessageLookupByLibrary.simpleMessage("OK"),
        "city_of_residence_content_text":
            MessageLookupByLibrary.simpleMessage("千代田区"),
        "completed": MessageLookupByLibrary.simpleMessage("完了"),
        "confirm_delete_account":
            MessageLookupByLibrary.simpleMessage("アカウントを削除しますか?"),
        "confirm_exit": MessageLookupByLibrary.simpleMessage("本当にログアウトしますか?"),
        "confirm_open_detail_honda_project":
            MessageLookupByLibrary.simpleMessage("使用済となりますがよろしいですか？"),
        "content_history_screen":
            MessageLookupByLibrary.simpleMessage("4回目の来店でした。"),
        "date_content_history_screen":
            MessageLookupByLibrary.simpleMessage("2023年8月11日"),
        "do_not_show_next_time":
            MessageLookupByLibrary.simpleMessage("次回から表示しない"),
        "drawer_email": MessageLookupByLibrary.simpleMessage("お問い合わせ"),
        "drawer_guide": MessageLookupByLibrary.simpleMessage("Giftアプリの使い方"),
        "drawer_logout": MessageLookupByLibrary.simpleMessage("ログアウト"),
        "drawer_person": MessageLookupByLibrary.simpleMessage("アカウント情報"),
        "email_address": MessageLookupByLibrary.simpleMessage("メールアドレス"),
        "era": MessageLookupByLibrary.simpleMessage("年代(任意)"),
        "era_require": MessageLookupByLibrary.simpleMessage("年代"),
        "expiration_date":
            MessageLookupByLibrary.simpleMessage("有効期限：2023年8月11日"),
        "gift_taro_text": MessageLookupByLibrary.simpleMessage("ギフたろー"),
        "girl": MessageLookupByLibrary.simpleMessage("女性"),
        "go_to_influencer_list":
            MessageLookupByLibrary.simpleMessage("インフルエンサー一覧をみる"),
        "guide_tab_page_title": MessageLookupByLibrary.simpleMessage("アプリの使い方"),
        "guide_when_tap_honda_project_list":
            MessageLookupByLibrary.simpleMessage("お店のNFCシールにタッチしてね"),
        "history_near_shop_list_title":
            MessageLookupByLibrary.simpleMessage("おすすめのショップ"),
        "history_page_title": MessageLookupByLibrary.simpleMessage("ホーム"),
        "honda_project_detail_page_title":
            MessageLookupByLibrary.simpleMessage("クーポン"),
        "honda_project_expired":
            MessageLookupByLibrary.simpleMessage("有効期限が切れています"),
        "honda_project_page_title":
            MessageLookupByLibrary.simpleMessage("honda_project"),
        "honda_project_tab_page_header":
            MessageLookupByLibrary.simpleMessage("ショップクーポン"),
        "honda_project_time_remain":
            MessageLookupByLibrary.simpleMessage("残り有効期間："),
        "honda_project_view_only_suggest_title":
            MessageLookupByLibrary.simpleMessage(
                "アカウントを登録して\nお得なクーポンを\nゲットしよう！\n"),
        "how_to_use": MessageLookupByLibrary.simpleMessage("アプリの使い方"),
        "if_you_forget_your_password":
            MessageLookupByLibrary.simpleMessage("パスワードを忘れた場合"),
        "influencer": MessageLookupByLibrary.simpleMessage("インフルエンサー"),
        "influencer_confirm_message":
            MessageLookupByLibrary.simpleMessage("このインフルエンサーにいいねを送りますか？"),
        "influencer_confirm_note":
            MessageLookupByLibrary.simpleMessage("他のインフルエンサーにいいねが送れなくなります。"),
        "invitation_code_text":
            MessageLookupByLibrary.simpleMessage("招待コード(任意)"),
        "lets_enjoy_gift":
            MessageLookupByLibrary.simpleMessage("Let\'s enjoy gift"),
        "login": MessageLookupByLibrary.simpleMessage("ログイン"),
        "login_header_gif": MessageLookupByLibrary.simpleMessage("CMN Gift"),
        "login_header_honda_project":
            MessageLookupByLibrary.simpleMessage("Gift"),
        "name_account": MessageLookupByLibrary.simpleMessage("ニックネーム"),
        "name_account_content_text":
            MessageLookupByLibrary.simpleMessage("ギフトたろー"),
        "near_shop_list": MessageLookupByLibrary.simpleMessage("近くのショップ "),
        "next": MessageLookupByLibrary.simpleMessage("次へ"),
        "nice": MessageLookupByLibrary.simpleMessage("いいね"),
        "nick_name": MessageLookupByLibrary.simpleMessage("gift-taro"),
        "no_connection_message": MessageLookupByLibrary.simpleMessage(
            "インターネット接続がありません。インターネット接続を確認してからもう一度お試しください。"),
        "no_reply": MessageLookupByLibrary.simpleMessage("回答しない"),
        "not_select": MessageLookupByLibrary.simpleMessage("選択しない"),
        "notice": MessageLookupByLibrary.simpleMessage("お知らせ"),
        "ok": MessageLookupByLibrary.simpleMessage("OK"),
        "password": MessageLookupByLibrary.simpleMessage("パスワード"),
        "prefectures_content_text": MessageLookupByLibrary.simpleMessage("東京都"),
        "privacy_policy": MessageLookupByLibrary.simpleMessage("プライバシーポリシー"),
        "privacy_policy_text":
            MessageLookupByLibrary.simpleMessage("プライバシポリシーに"),
        "profile": MessageLookupByLibrary.simpleMessage("プロフィール"),
        "re_password": MessageLookupByLibrary.simpleMessage("パスワード確認"),
        "register_add_account": MessageLookupByLibrary.simpleMessage(
            "アプリをより使いやすくするための項目を設定し\nてください。"),
        "register_button": MessageLookupByLibrary.simpleMessage("利用規約"),
        "register_button_phone_number_text":
            MessageLookupByLibrary.simpleMessage("SMSに認証コードを送る"),
        "register_content_mobile_number": MessageLookupByLibrary.simpleMessage(
            "電話番号は本人認証で使用するためで、\nデータとして保持することはありません。\n\n※通信料はお客様のご負担となります"),
        "register_content_user_code":
            MessageLookupByLibrary.simpleMessage("半角英字、数字が使用できます。"),
        "register_example":
            MessageLookupByLibrary.simpleMessage("例:08011112222"),
        "register_final_page": MessageLookupByLibrary.simpleMessage("アプリの使い方"),
        "register_final_page_button":
            MessageLookupByLibrary.simpleMessage("アプリを使う"),
        "register_input_phone_number":
            MessageLookupByLibrary.simpleMessage("電話番号を再入力する"),
        "register_input_phone_number_text":
            MessageLookupByLibrary.simpleMessage("携帯番号"),
        "register_mobile_number_text":
            MessageLookupByLibrary.simpleMessage("携帯番号を入力してください。"),
        "register_page_content_title": MessageLookupByLibrary.simpleMessage(
            "当アプリのアカウント登録においては個人情報の提供は一切必要ありません。\n個人情報を登録しないようお願いします。"),
        "register_password_title":
            MessageLookupByLibrary.simpleMessage("ログインに使用するパスワードを入力してください。"),
        "register_sms_code_text": m0,
        "register_sms_content_text":
            MessageLookupByLibrary.simpleMessage("1分たっても認証番号が届かない方へ"),
        "register_user_code": MessageLookupByLibrary.simpleMessage("ユーザーコード"),
        "register_user_code_page_content_title":
            MessageLookupByLibrary.simpleMessage("ログインに使用するユーザーコードの入力してください。"),
        "reload": MessageLookupByLibrary.simpleMessage("リロード"),
        "require_not_empty": MessageLookupByLibrary.simpleMessage("必須項目です"),
        "residence_address_text":
            MessageLookupByLibrary.simpleMessage("東京都千代田区"),
        "residence_text": MessageLookupByLibrary.simpleMessage("お住まい"),
        "see_all_near_shop": MessageLookupByLibrary.simpleMessage("全て見る"),
        "send_a_like_content": MessageLookupByLibrary.simpleMessage(
            "お店を紹介してくれたインフルエンサーや好きなインフルエンサーに\n「いいね」を送ろう"),
        "send_a_like_title": MessageLookupByLibrary.simpleMessage("いいねを送ろう！"),
        "sex": MessageLookupByLibrary.simpleMessage("性別"),
        "start_a_gift": MessageLookupByLibrary.simpleMessage("giftをはじめる"),
        "term_of_use": MessageLookupByLibrary.simpleMessage("利用規約"),
        "terms_of_service_text": MessageLookupByLibrary.simpleMessage("利用規約"),
        "thank_message":
            MessageLookupByLibrary.simpleMessage("ご利用ありがとうございました。"),
        "times_for_user": MessageLookupByLibrary.simpleMessage("お一人様一回まで"),
        "title_button_item_honda_project":
            MessageLookupByLibrary.simpleMessage("クーポンGet"),
        "title_content_history_screen":
            MessageLookupByLibrary.simpleMessage("KANNON COFFEE 松陰神社店"),
        "title_content_item_honda_project":
            MessageLookupByLibrary.simpleMessage("割引クーポン"),
        "unknown_error": MessageLookupByLibrary.simpleMessage("不明なエラー"),
        "user_code": MessageLookupByLibrary.simpleMessage("ユーザーコード"),
        "view_image": MessageLookupByLibrary.simpleMessage("画像を表示"),
        "visit_history_title": MessageLookupByLibrary.simpleMessage("来店履歴"),
        "when_you_create_an_account_text":
            MessageLookupByLibrary.simpleMessage("アカウントを作成すると"),
        "you_agree_to_text":
            MessageLookupByLibrary.simpleMessage("同意したことになります。"),
        "you_live_prefectures":
            MessageLookupByLibrary.simpleMessage("お住まいの都道府県")
      };
}
