
import 'package:demo_mvvm_flutter/model/http_raw/HttpRaw.dart';

class NetworkException {
  final int? code;
  final String? message;

  const NetworkException({this.code, this.message});

  factory NetworkException.copyFromHttpRaw(HttpRaw httpRaw) {
    return NetworkException(
      code: httpRaw.errorCode,
      message: httpRaw.errorMessage,
    );
  }
}