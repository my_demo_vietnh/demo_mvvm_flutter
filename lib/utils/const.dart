

import 'package:flutter/material.dart';

class Const {
  static const screenDesignSize = Size(390, 844);

  static const String baseUrlStg = "https://fc5bdab0-1a49-4f69-a1b1-77e75dfa74f9.mock.pstmn.io/";
  static const String baseUrlProduct = "";



  /// Code response API
  static const int successNetworkCall = 200;
  static const int unknownErrorNetworkCall = 1001;
  static const int noInternet = 1000;

  static const int forceUpdateResponseCode = 999;
  static const int forceLogoutResponseCode = 1000;
  static const int error401ResponseCode = 401;
  static const int error400ResponseCode = 400;
  static const int error403ResponseCode = 403;
  static const int error500ResponseCode = 500;
  static const int maintenanceResponseCode = 600;

  /// Parsing net  response error
  static const int parsingNetworkResponseError = 700;
}